package manachem.titouan.cms.tpl

import manachem.titouan.cms.model.Article

data class IndexContext (
    val list: List<Article>
)