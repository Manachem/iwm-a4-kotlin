package manachem.titouan.cms

import io.ktor.http.Parameters
import manachem.titouan.cms.model.Article
import manachem.titouan.cms.model.Commentaire
import manachem.titouan.cms.model.Utilisateur

interface AdminPresenter{

    fun start(){
    }
    fun deleteCommentaire(id:Int){
    }

    fun deleteArticle(id:Int){
    }

    fun addArticle(title : String , desc: String){
    }

    interface View {
        fun displayAdmin(list : List<Article>?)
    }
}