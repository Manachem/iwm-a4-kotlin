package manachem.titouan.cms

import manachem.titouan.cms.model.Article
import manachem.titouan.cms.model.Commentaire
import manachem.titouan.cms.model.Utilisateur
import manachem.titouan.cms.model.UtilisateurSession

interface Model {

    fun getArticleList(): List<Article>

    fun getArticle(id: Int): Article?

    fun getCommentaireList(id: Int): List<Commentaire>?

    fun addCommentaire(idArticle: Int, text: String)

    fun getUtilisateur(username: String, password: String): Utilisateur?

    fun isAdmin(userSession: UtilisateurSession?): Boolean

    fun deleteCommentaire(idCommentaire: Int)

    fun deleteArticle(id: Int)

    fun addArticle(title : String , desc: String)
}