package manachem.titouan.cms

import io.ktor.http.Parameters
import manachem.titouan.cms.model.Article
import manachem.titouan.cms.model.Commentaire
import manachem.titouan.cms.model.Utilisateur

interface LoginPresenter{

    fun start(){
    }

    fun login(password:String,username:String){

    }

    interface View {
        fun displayLogin()
        fun displaySuccessLogin(user: Utilisateur)
    }
}