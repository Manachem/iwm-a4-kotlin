package manachem.titouan.cms.control

import manachem.titouan.cms.ArticlePresenter
import manachem.titouan.cms.Model

class ArticlePresenterImpl(val model:Model, val view : ArticlePresenter.View) : ArticlePresenter {
    override fun start(id : Int){

        //Récupération des commentaires et de l'article
        var article = model.getArticle(id)

        if(article != null ){
            view.displayArticle(article)
        } else {
            view.displayNotFound()
        }
    }

    override fun addCommentaire(id : Int, postCommentaire : String?){
        //Récupération des commentaires et de l'article
        var article = model.getArticle(id)

        if(article != null ){
            //Insertion du nouveau commentaire
            if (postCommentaire != null){
                if( postCommentaire != ""){
                    model.addCommentaire(id,  postCommentaire )
                }
            }
            view.displayArticle(article)
        } else {
            view.displayNotFound()
        }
    }
}