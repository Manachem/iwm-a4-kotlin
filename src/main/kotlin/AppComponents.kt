import manachem.titouan.cms.*
import manachem.titouan.cms.control.AdminPresenterImpl
import manachem.titouan.cms.control.ArticleListPresenterImpl
import manachem.titouan.cms.control.ArticlePresenterImpl
import manachem.titouan.cms.control.LoginPresenterImpl

class  AppComponents(mySqlUrl : String, val mySqlUser: String, val mySqlPassword: String){

    private val pool = ConnectionPool(mySqlUrl,mySqlUser,mySqlPassword)

    fun getPool(): ConnectionPool {
        return pool
    }

    private val model = MysqlModel(getPool())

    fun getModel(): Model {
        return model
    }

    fun getArticleListPresenter(view: ArticleListPresenter.View): ArticleListPresenter {
        return ArticleListPresenterImpl(getModel(), view)
    }

    fun getArticlePresenter(view: ArticlePresenter.View): ArticlePresenter {
        return ArticlePresenterImpl(getModel(),view)
    }

    fun getLoginPresenter(view: LoginPresenter.View): LoginPresenter {
        return LoginPresenterImpl(getModel(),view)
    }

    fun getAdminPresenter(view: AdminPresenter.View): AdminPresenter {
        return AdminPresenterImpl(getModel(),view)
    }
}