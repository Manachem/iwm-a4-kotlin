package manachem.titouan.cms.control
import manachem.titouan.cms.ArticleListPresenter
import manachem.titouan.cms.Model


class ArticleListPresenterImpl(val model: Model, val view : ArticleListPresenter.View) : ArticleListPresenter {
    override fun start(){
        var list = model.getArticleList()
        view.displayArticleList(list)
    }
}