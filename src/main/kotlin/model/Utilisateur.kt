package manachem.titouan.cms.model

data class Utilisateur (
    val id: Int,
    val Username: String,
    val Password: String,
    val Role : List<String>,
    val Token :  String
)