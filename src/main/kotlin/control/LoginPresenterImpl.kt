package manachem.titouan.cms.control

import io.ktor.freemarker.FreeMarkerContent
import io.ktor.http.Parameters
import manachem.titouan.cms.LoginPresenter
import manachem.titouan.cms.Model


class LoginPresenterImpl(val model:Model, val view : LoginPresenter.View) : LoginPresenter {
    override fun start(){
        view.displayLogin()
    }

    override fun login(password: String, username: String) {
        //Check login
        val user = model.getUtilisateur(password, username)
        if (user != null) {
            view.displaySuccessLogin(user)
        } else {
            view.displayLogin()
        }
    }
}