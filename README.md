# IWM-A4-KOTLIN

Projet IWM année 4 - Cours Kotlin


Identifiant de connexion :

username :  admin
password : admin


Fonctionnalités :

- Style (moche) via CSS
- Affichage des commentaires d'un article (table commentaire : id, idArticle, text).
- Possibilite de poster un commentaire (sans etre connecté) depuis la page d'un article.
- Connection a une interface d'administration avec login / mot de passe.
- Gestion de la session, possibilité de se déconnecter.
- Une fois connecté en tant qu'admin, possibilité d'ajouter ou supprimer un article.
- Si on supprime un article, ses commentaires doivent etre supprimés (soit manuellement, soit via les MySQL Foreign keys).
- Une fois connecté en tant qu'admin, possibilité de supprimer un commentaire.
- Les presenters correspondant a ces nouvelles fonctionalités doivent etre correctement testés.

Fonctionnalités bonus :

- Gestion des utilisateurs et de leurs droits