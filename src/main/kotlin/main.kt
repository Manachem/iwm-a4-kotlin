import manachem.titouan.cms.model.Article
import manachem.titouan.cms.tpl.IndexContext
import manachem.titouan.cms.model.Commentaire

import freemarker.cache.ClassTemplateLoader
import io.ktor.application.ApplicationCallPipeline
import kotlinx.coroutines.launch
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.*
import io.ktor.freemarker.FreeMarker
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.http.HttpStatusCode
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.response.respond
import io.ktor.routing.route
import io.ktor.http.Parameters
import io.ktor.http.content.resources
import io.ktor.http.content.static
import io.ktor.request.*
import io.ktor.response.respondRedirect
import io.ktor.response.respondText
import io.ktor.routing.*
import io.ktor.sessions.*
import manachem.titouan.cms.AdminPresenter
import manachem.titouan.cms.ArticleListPresenter
import manachem.titouan.cms.ArticlePresenter
import manachem.titouan.cms.LoginPresenter
import manachem.titouan.cms.model.Utilisateur
import manachem.titouan.cms.model.UtilisateurSession

class App

fun main() {

    val appComponents = AppComponents("jdbc:mysql://localhost/kotlin?serverTimezone=UTC", "root", "")

    embeddedServer(Netty, 8090) {
        install(FreeMarker) {
            templateLoader = ClassTemplateLoader(App::class.java.classLoader, "templates")
        }
        install(Sessions) {
            cookie<UtilisateurSession>("SESSION")
        }

        routing {
            get("/") {
                val session : UtilisateurSession? = call.sessions.get<UtilisateurSession>()
                val controller = appComponents.getArticleListPresenter(object: ArticleListPresenter.View {
                    override fun displayArticleList( list : List<Article>) {
                        val articles = IndexContext(list)
                        val isAdmin = appComponents.getModel().isAdmin(session)
                        launch {
                            call.respond(FreeMarkerContent("index.ftl",  mapOf("articles" to articles, "session" to session, "isAdmin" to isAdmin), "e"))
                        }
                    }
                })
                controller.start()
            }

            route("/login"){
                get {
                    val controller = appComponents.getLoginPresenter(object: LoginPresenter.View {
                        override fun displayLogin() {
                            launch {
                                call.respond(FreeMarkerContent("login.ftl", null, "e"))
                            }
                        }
                        override fun displaySuccessLogin(user : Utilisateur) {}
                    })
                    controller.start()
                }

                post {
                    val params = call.receive<Parameters>()
                    val controller = appComponents.getLoginPresenter(object: LoginPresenter.View {
                        override fun displayLogin() {
                            launch {
                                call.respond(FreeMarkerContent("login.ftl", mapOf("error" to "Invalid login")))
                            }
                        }
                        override fun displaySuccessLogin(user : Utilisateur) {
                            call.sessions.set("SESSION", UtilisateurSession(user.Token))
                            launch {
                                call.respondRedirect("/", permanent = false)
                            }
                        }

                    })
                    if( params["username"] != null && params["password"] != null ){
                        controller.login( params["password"]!!, params["username"]!!)
                    } else {
                        controller.start()
                    }
                }
            }


            route("/article") {
                get("{id}") {
                    val id = call.parameters["id"]!!.toIntOrNull()

                    val controller = appComponents.getArticlePresenter(object: ArticlePresenter.View {
                        override fun displayArticle( article : Article) {
                            launch {
                                call.respond(FreeMarkerContent("article.ftl", mapOf("article" to article), "e"))
                            }
                        }
                        override fun displayNotFound() {
                            launch {
                                call.respond(HttpStatusCode.NotFound)
                            }
                        }
                    })
                    controller.start(id!!)
                }

                post("{id}") {
                    val id = call.parameters["id"]!!.toIntOrNull()
                    val params = call.receive<Parameters>()

                    val controller = appComponents.getArticlePresenter(object: ArticlePresenter.View {
                        override fun displayArticle( article : Article) {
                            launch {
                                call.respondRedirect("/article/"+id, permanent = false)
                            }
                        }
                        override fun displayNotFound() {
                            launch {
                                call.respondRedirect("/article/"+id, permanent = false)
                            }
                        }
                    })
                    controller.addCommentaire(id!!, params["text"])
                }
            }

            route("/logout"){
                get("") {
                    call.sessions.clear<UtilisateurSession>()
                    launch {
                        call.respondRedirect("/", permanent = false)
                    }
                }
            }


            route("/admin"){

                //firewall admin
                intercept(ApplicationCallPipeline.Features) {
                    if (call.request.path().split('/').contains("admin")) {
                        val session : UtilisateurSession? = call.sessions.get<UtilisateurSession>()
                        val isAdmin = appComponents.getModel().isAdmin(session)
                        if( !isAdmin ){
                            launch {
                                call.respondRedirect("/login", permanent = false)
                            }
                            return@intercept finish()
                        }
                    }
                }

                get("") {

                    val controller = appComponents.getAdminPresenter(object: AdminPresenter.View {
                        override fun displayAdmin( list : List<Article>? ) {
                            val articles = IndexContext(list!!)
                            launch {
                                call.respond(FreeMarkerContent("admin.ftl",  mapOf("articles" to articles), "e"))
                            }
                        }
                    })
                    controller.start()
                }

                post("") {
                    val params = call.receive<Parameters>()
                    val controller = appComponents.getAdminPresenter(object: AdminPresenter.View {
                        override fun displayAdmin( list : List<Article>? ) {
                            val articles = IndexContext(list!!)
                            launch {
                                call.respond(FreeMarkerContent("admin.ftl",  mapOf("articles" to articles), "e"))
                            }
                        }
                    })
                    if(params["title"] != null && params["desc"] != null ){
                        controller.addArticle(params["title"]!!, params["desc"]!!)
                    } else {
                        controller.start()
                    }

                }

                get("commentaire/delete/{id}") {
                    val id = call.parameters["id"]!!.toIntOrNull()
                    val controller = appComponents.getAdminPresenter(object: AdminPresenter.View {
                        override fun displayAdmin( list : List<Article>? ) {
                            launch {
                                call.respondRedirect("/admin", permanent = false)
                            }
                        }
                    })
                    controller.deleteCommentaire(id!!)
                }

                get("article/delete/{id}") {
                    val id = call.parameters["id"]!!.toIntOrNull()
                    val controller = appComponents.getAdminPresenter(object: AdminPresenter.View {
                        override fun displayAdmin( list : List<Article>? ) {
                            launch {
                                call.respondRedirect("/admin", permanent = false)
                            }
                        }
                    })
                    controller.deleteArticle(id!!)
                }
            }


            static("/assets") {
                resources("assets")
            }




        }
    }.start(wait = true)
}