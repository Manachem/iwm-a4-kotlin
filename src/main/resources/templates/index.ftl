<html>
    <head>
        <link rel="stylesheet" href="/assets/style.css">
    </head>

    <body>
        <div class="container">
            <h1>Home</h1>
            <div class='spacer'>
                <#if session??>
                    <a href="/logout" class='wide blue'>
                        <i class="icon-home"></i>
                        <h2>Logout</h2>
                    </a>
                    <#else>
                    <a href="/login" class='wide blue'>
                        <i class="icon-home"></i>
                        <h2>Login</h2>
                    </a>
                </#if>

                <#if isAdmin >
                    <a href="/admin" class='box orange'>
                        <i class="icon-cog"></i>
                        <h2>Panel Admin</h2>
                    </a>
                </#if>

                <!-- DECORATION -->
                <div class='box redgay innactif'>
                    <i class="icon-camera"></i>
                    <h2>Camera</h2>
                </div>
                <div class='box lime innactif'>
                    <i class="icon-heart"></i>
                    <h2>Favorite</h2>
                </div>
                <div class='box bluefish innactif'>
                    <i class="icon-twitter"></i>
                    <h2>Twitter</h2>
                </div>
                <div class='box yellow innactif'>
                    <i class="icon-map-marker"></i>
                    <h2>Places</h2>
                </div>
                <div class='wide orange innactif'>
                    <i class="icon-cloud"></i>
                    <h2>Gallery</h2>
                </div>
                <div class='box redgay innactif'>
                    <i class="icon-globe"></i>
                    <h2>Browser</h2>
                </div>
                <div class='box orange innactif'>
                    <i class="icon-envelope-alt"></i>
                    <h2>E-mail</h2>
                </div>
                <div class='wide blue cal_e innactif'>
                    <h1>25</h1>
                    <p>Monday</p>
                    <h2 class="top cal_i">Sena Birthday Party At Jack House on 07:00 PM</h2>
                    <i class="icon-calendar"></i>
                </div>
                <div class='box lime innactif'>
                    <i class="icon-cloud"></i>
                    <h2>SkyDrive</h2>
                </div>
                <!-- FIN DECORATION -->
            </div>

            <div class='spacer spacer3x'>
                <h2 class="articles">Articles</h2>
                <#list articles.list as article>
                    <a href="/article/${article.id}" class='wide orange'>
                        <i class="icon-heart"></i>
                        <h2>${article.title}</h2>
                    </a>
                </#list>
            </div>
        </div>
    </body>
</html>