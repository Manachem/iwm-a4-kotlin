package manachem.titouan.cms

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import io.ktor.http.Parameters
import manachem.titouan.cms.control.AdminPresenterImpl
import manachem.titouan.cms.control.ArticleListPresenterImpl
import manachem.titouan.cms.control.ArticlePresenterImpl
import manachem.titouan.cms.control.LoginPresenterImpl
import manachem.titouan.cms.model.Article
import manachem.titouan.cms.model.Commentaire
import manachem.titouan.cms.model.Utilisateur
import org.junit.Assert.*
import org.junit.Test

class PresenterTests {

    @Test
    fun testArticleListPresenters()
    {

        val list = listOf(Article(1, "Un", null,null), Article(2, "Un", null,null))

        val model = mock<Model> {
            on { getArticleList() } doReturn list
        }

        val view = mock<ArticleListPresenter.View>()

        val presenter = ArticleListPresenterImpl(model, view)
        presenter.start()

        verify(model).getArticleList()
        verify(view).displayArticleList(list)
        verifyNoMoreInteractions(model, view)
    }

    @Test
    fun testValidArticlePresenter() {
        val comments = listOf(Commentaire(1,42,"test comment"),Commentaire(2,42,"test comment 2"))
        val article = Article(42, "Quarante-deux", "Lorem ipsum",comments )

        val model = mock<Model> {
            on { getArticle(42) } doReturn article
        }

        val view = mock<ArticlePresenter.View>()

        val presenter = ArticlePresenterImpl(model, view)
        presenter.start(42)

        verify(model).getArticle(42)
        verify(view).displayArticle(article)
        verifyNoMoreInteractions(model, view)
    }

    @Test
    fun testValidArticleWithoutCommentPresenter() {
        val article = Article(42, "Quarante-deux", "Lorem ipsum",null )

        val model = mock<Model> {
            on { getArticle(42) } doReturn article
        }

        val view = mock<ArticlePresenter.View>()

        val presenter = ArticlePresenterImpl(model, view)
        presenter.start(42)

        verify(model).getArticle(42)
        verify(view).displayArticle(article)
        verifyNoMoreInteractions(model, view)
    }

    @Test
    fun testInvalidArticlePresenter() {

        val model = mock<Model> {
            on { getArticle(40) } doReturn null
        }

        val view = mock<ArticlePresenter.View>()

        val presenter = ArticlePresenterImpl(model, view)
        presenter.start(40)

        verify(model).getArticle(40)
        verify(view).displayNotFound()
        verifyNoMoreInteractions(model, view)
    }


    @Test
    fun testAddCommentArticlePresenter() {

        val article = Article(42, "Quarante-deux", "Lorem ipsum",null )
        val model = mock<Model> {
            on { getArticle(40) } doReturn article
        }

        val view = mock<ArticlePresenter.View>()

        val presenter = ArticlePresenterImpl(model, view)
        presenter.addCommentaire(40, "test")


        verify(model).getArticle(40)
        verify(model).addCommentaire(40,"test")
        verify(view).displayArticle(article)
        verifyNoMoreInteractions(model, view)
    }

    @Test
    fun testAddCommentinvalidArticlePresenter() {

        val model = mock<Model> {
            on { getArticle(40) } doReturn null
        }

        val view = mock<ArticlePresenter.View>()

        val presenter = ArticlePresenterImpl(model, view)
        presenter.addCommentaire(40, "test")

        verify(model).getArticle(40)
        verify(view).displayNotFound()
        verifyNoMoreInteractions(model, view)
    }

    @Test
    fun testAddInvalidCommentArticlePresenter() {

        val article = Article(42, "Quarante-deux", "Lorem ipsum",null )
        val model = mock<Model> {
            on { getArticle(42) } doReturn article
        }

        val view = mock<ArticlePresenter.View>()

        val presenter = ArticlePresenterImpl(model, view)
        presenter.addCommentaire(42, null)

        verify(model).getArticle(42)
        verify(view).displayArticle(article)
        verifyNoMoreInteractions(model, view)
    }


    @Test
    fun testLoginPresenter() {

        val model = mock<Model> {
        }

        val view = mock<LoginPresenter.View>()

        val presenter = LoginPresenterImpl(model, view)
        presenter.start()

        verify(view).displayLogin()
        verifyNoMoreInteractions(model, view)
    }

    @Test
    fun testFailedLoginPresenter() {
        val model = mock<Model> {
            on { getUtilisateur("test","test") } doReturn null
        }
        val view = mock<LoginPresenter.View>()
        val presenter = LoginPresenterImpl(model, view)
        presenter.login("test","test")
        verify(model).getUtilisateur("test","test")
        verify(view).displayLogin()
        verifyNoMoreInteractions(model, view)
    }

    @Test
    fun testSucessLoginPresenter() {
        val user = Utilisateur(1,"test","test",listOf("test"),"tokenTest")
        val model = mock<Model> {
            on { getUtilisateur("test","test") } doReturn user
        }
        val view = mock<LoginPresenter.View>()
        val presenter = LoginPresenterImpl(model, view)
        presenter.login("test","test")
        verify(model).getUtilisateur("test","test")
        verify(view).displaySuccessLogin(user)
        verifyNoMoreInteractions(model, view)
    }

    @Test
    fun testAdminPresenter() {
        val list = listOf(Article(1, "Un", null,null), Article(2, "Un", null,null))

        val model = mock<Model> {
            on { getArticleList() } doReturn list
        }
        val view = mock<AdminPresenter.View>()
        val presenter = AdminPresenterImpl(model, view)
        presenter.start()
        verify(model).getArticleList()
        verify(view).displayAdmin(list)
        verifyNoMoreInteractions(model, view)
    }

    @Test
    fun testDeleteCommentPresenter() {
        val list = listOf(Article(1, "Un", null,listOf(Commentaire(1,1,"test"))), Article(2, "Un", null,null))
        val model = mock<Model> {
            on { getArticleList() } doReturn list
        }
        val view = mock<AdminPresenter.View>()
        val presenter = AdminPresenterImpl(model, view)
        presenter.deleteCommentaire(1)
        verify(model).getArticleList()
        verify(model).deleteCommentaire(1)
        verify(view).displayAdmin(list)
        verifyNoMoreInteractions(model, view)
    }

    @Test
    fun testDeleteInvalidCommentPresenter() {
        val list = listOf(Article(1, "Un", null,null), Article(2, "Un", null,null))
        val model = mock<Model> {
            on { getArticleList() } doReturn list
        }
        val view = mock<AdminPresenter.View>()
        val presenter = AdminPresenterImpl(model, view)
        presenter.deleteCommentaire(-1)
        verify(model).getArticleList()
        verify(model).deleteCommentaire(-1)
        verify(view).displayAdmin(list)
        verifyNoMoreInteractions(model, view)
    }

    @Test
    fun testDeleteArticlePresenter() {
        val list = listOf(Article(1, "Un", null,null), Article(2, "Un", null,null))
        val model = mock<Model> {
            on { getArticleList() } doReturn list
        }
        val view = mock<AdminPresenter.View>()
        val presenter = AdminPresenterImpl(model, view)
        presenter.deleteArticle(1)
        verify(model).getArticleList()
        verify(model).deleteArticle(1)
        verify(view).displayAdmin(list)
        verifyNoMoreInteractions(model, view)
    }

    @Test
    fun testDeleteInvalidArticlePresenter() {
        val list = listOf(Article(1, "Un", null,null), Article(2, "Un", null,null))
        val model = mock<Model> {
            on { getArticleList() } doReturn list
        }
        val view = mock<AdminPresenter.View>()
        val presenter = AdminPresenterImpl(model, view)
        presenter.deleteArticle(-1)
        verify(model).getArticleList()
        verify(model).deleteArticle(-1)
        verify(view).displayAdmin(list)
        verifyNoMoreInteractions(model, view)
    }

    @Test
    fun testDeleteInvalidArticlePresenterV2() {
        val list = listOf(Article(1, "Un", null,null), Article(2, "Un", null,null))
        val model = mock<Model> {
            on { getArticleList() } doReturn list
        }
        val view = mock<AdminPresenter.View>()
        val presenter = AdminPresenterImpl(model, view)
        presenter.deleteArticle(9)
        verify(model).getArticleList()
        verify(model).deleteArticle(9)
        verify(view).displayAdmin(list)
        verifyNoMoreInteractions(model, view)
    }

    @Test
    fun testAddArticlePresenter() {
        val list = listOf(Article(1, "Un", null,null), Article(2, "Un", null,null))
        val model = mock<Model> {
            on { getArticleList() } doReturn list
        }
        val view = mock<AdminPresenter.View>()
        val presenter = AdminPresenterImpl(model, view)
        presenter.addArticle("test","test")
        verify(model).addArticle("test","test")
        verify(model).getArticleList()
        verify(view).displayAdmin(list)
        verifyNoMoreInteractions(model, view)
    }




}