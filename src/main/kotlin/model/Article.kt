package manachem.titouan.cms.model

data class Article (
    val id: Int,
    val title: String,
    val text: String?,
    val commentaires: List<Commentaire>?
)