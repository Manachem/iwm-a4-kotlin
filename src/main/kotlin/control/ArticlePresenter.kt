package manachem.titouan.cms

import manachem.titouan.cms.model.Article
import manachem.titouan.cms.model.Commentaire

interface ArticlePresenter{

    fun start(id : Int ){
    }

    fun addCommentaire(id : Int, postCommentaire : String?){
    }

    interface View {
        fun displayArticle(article : Article)
        fun displayNotFound()
    }
}