package manachem.titouan.cms.control

import io.ktor.freemarker.FreeMarkerContent
import io.ktor.http.Parameters
import manachem.titouan.cms.AdminPresenter
import manachem.titouan.cms.LoginPresenter
import manachem.titouan.cms.Model


class AdminPresenterImpl(val model:Model, val view : AdminPresenter.View) : AdminPresenter {

    override fun start(){
        var list = model.getArticleList()
        view.displayAdmin(list)
    }

    override fun addArticle(title : String , desc: String){
        model.addArticle(title,desc)
        var list = model.getArticleList()
        view.displayAdmin(list)
    }

    override fun deleteCommentaire(id:Int){
        model.deleteCommentaire(id)
        var list = model.getArticleList()
        view.displayAdmin(list)
    }

    override fun deleteArticle(id:Int){
        model.deleteArticle(id)
        var list = model.getArticleList()
        view.displayAdmin(list)
    }
}