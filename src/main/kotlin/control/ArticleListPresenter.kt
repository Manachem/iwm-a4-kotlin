package manachem.titouan.cms

import manachem.titouan.cms.model.Article

interface ArticleListPresenter{

    fun start(){

    }

    interface View {
        fun displayArticleList(list : List<Article>)
    }
}