import manachem.titouan.cms.ConnectionPool
import manachem.titouan.cms.Model
import manachem.titouan.cms.model.Article
import manachem.titouan.cms.model.Commentaire
import manachem.titouan.cms.model.Utilisateur
import manachem.titouan.cms.model.UtilisateurSession
import java.awt.SystemColor.text

class MysqlModel(val pool: ConnectionPool): Model {

    override fun getArticleList(): List<Article> {
        val list = ArrayList<Article>()
        pool.useConnection { connection ->
            connection.prepareStatement("SELECT * FROM article").use { stmt ->
                stmt.executeQuery().use { results ->
                    while (results.next()) {
                        list += Article(
                            results.getInt("id"),
                            results.getString("title"),
                            results.getString("text"),
                            getCommentaireList(results.getInt("id"))
                        )
                    }
                }
            }
        }
        return list
    }

    override fun getArticle(id: Int): Article? {
        pool.useConnection { connection ->
            connection.prepareStatement("SELECT * FROM article WHERE id = ?").use { stmt ->
                stmt.setInt(1, id)
                stmt.executeQuery().use { result ->
                    if (result.next()) {
                        return Article(
                            result.getInt("id"),
                            result.getString("title"),
                            result.getString("text"),
                            getCommentaireList(result.getInt("id"))
                        )
                    }
                }
            }
        }
        return null
    }

    override fun getCommentaireList(id: Int): List<Commentaire>? {
        val list = ArrayList<Commentaire>()
        pool.useConnection { connection ->
            connection.prepareStatement("SELECT * FROM commentaire WHERE idArticle = ?").use { stmt ->
                stmt.setInt(1, id)
                stmt.executeQuery().use { results ->
                    while (results.next()) {
                        list += Commentaire(
                            results.getInt("id"),
                            results.getInt("idArticle"),
                            results.getString("text")
                        )
                    }
                }
            }
        }
        return list
    }

    override fun addCommentaire(id: Int, text: String) {
        pool.useConnection { connection ->
            connection.prepareStatement("INSERT INTO commentaire (idArticle,text) VALUES (?,?)").use { stmt ->
                stmt.setInt(1, id)
                stmt.setString(2, text)
                stmt.executeUpdate()
            }
        }
    }

    override fun deleteCommentaire(id: Int) {
        pool.useConnection { connection ->
            connection.prepareStatement("DELETE FROM commentaire WHERE id = ? ").use { stmt ->
                stmt.setInt(1, id)
                stmt.execute()
            }
        }
    }

    override fun addArticle(title : String , desc: String){
        pool.useConnection { connection ->
            connection.prepareStatement("INSERT INTO article (title ,text) VALUES (?,?)").use { stmt ->
                stmt.setString(1, title)
                stmt.setString(2, desc)
                stmt.executeUpdate()
            }
        }
    }

    override fun deleteArticle(id: Int) {
        pool.useConnection { connection ->
            connection.prepareStatement("DELETE FROM commentaire WHERE idArticle = ? ").use { stmt ->
                stmt.setInt(1, id)
                stmt.execute()
            }
            connection.prepareStatement("DELETE FROM article WHERE id = ? ").use { stmt ->
                stmt.setInt(1, id)
                stmt.execute()
            }
        }
    }

    override fun getUtilisateur(username: String, password: String): Utilisateur?{
        pool.useConnection { connection ->
            connection.prepareStatement("SELECT * FROM utilisateur WHERE username = ? AND password = ?").use { stmt ->
                stmt.setString(1, username )
                stmt.setString(2, password)
                stmt.executeQuery().use { results ->
                    while (results.next()) {
                        return Utilisateur(
                            results.getInt("id"),
                            results.getString("username"),
                            results.getString("password"),
                            results.getString("roles").split(" "),
                            results.getString("token")
                        )
                    }
                }
            }
        }
        return null;
    }

    override fun isAdmin(userSession: UtilisateurSession?): Boolean{
        if( userSession != null ){
            pool.useConnection { connection ->
                connection.prepareStatement("SELECT * FROM utilisateur WHERE token = ?").use { stmt ->
                    stmt.setString(1, userSession.Token )
                    stmt.executeQuery().use { results ->
                        while (results.next()) {
                            val user =  Utilisateur(
                                results.getInt("id"),
                                results.getString("username"),
                                results.getString("password"),
                                results.getString("roles").split(" "),
                                results.getString("token")
                            )

                            if( user.Role.contains("admin") ){
                                return true
                            } else {
                                return false
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

}