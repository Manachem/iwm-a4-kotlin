<html>
    <head>
        <link rel="stylesheet" href="/assets/style.css">
    </head>
    <body>
        <div class="container">
            <h1>${article.title}</h1>
            <div class='spacer'>
                <div class='wide blue cal_e ' style="position:relative;">
                    <h2>${article.text}</h2>
                </div>
            </div>
            <div class='spacer spacer3x'>
                <h2 class="articles">Comments</h2>
                <#list article.commentaires as commentaire>
                <div class='wide orange' style="position:relative;">
                    <h2>${commentaire.text}</h2>
                </div>
                </#list>
                <form action="" enctype="multipart/form-data" method="POST">
                    <h2 class="articles">Add a comment</h2>
                    <textarea name="text"></textarea>
                    <input type="submit" value="Add">
                </form>
            </div>
            <a href="/" class='box orange'>
                <i class="icon-home"></i>
                <h2>Home</h2>
            </a>
        </div>
    </body>
</html>