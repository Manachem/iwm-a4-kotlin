<html>
    <head>
        <link rel="stylesheet" href="/assets/style.css">
    </head>
    <body>

        <#if error??>
            <p style="color:red;">${error}</p>
        </#if>
        <#if success??>
            <p style="color:green;">${success}</p>
        </#if>

        <div class="container">
            <h1>Admin</h1>

            <div class='spacer'>

                <table>
                <#list articles.list as article>
                    <tr>
                        <td>
                            <a href="/article/${article.id}">${article.title}</a>
                        </td>
                        <td>
                            <a href="/admin/article/delete/${article.id}"> DELETE </a>
                        </td>
                        <td>

                        </td>
                    </tr>

                    <#list article.commentaires as commentaire>
                        <tr>
                            <td></td>
                            <td>
                                ${commentaire.text}
                            </td>
                            <td>
                                <a href="/admin/commentaire/delete/${commentaire.id}"> DELETE </a>
                            </td>
                        </tr>
                    </#list>
                </#list>
                </table>

                <br/>
                <a href="/" class='box orange'>
                    <i class="icon-home"></i>
                    <h2>Home</h2>
                </a>
            </div>

            <div class='spacer spacer3x'>
                <form method="POST">
                    <h2 class="articles" >Add Article</h2>
                    <label>Article title</label>
                    <input type="text" name="title"/><br/>
                    <label>Description title</label>
                    <input type="text" name="desc"/>
                    <input type="submit" name="validate" value="Add"/>
                </form>
            </div>
        </div>
    </body>
</html>