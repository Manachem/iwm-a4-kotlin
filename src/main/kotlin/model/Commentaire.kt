package manachem.titouan.cms.model

data class Commentaire (
    val id: Int,
    val idArticle: Int,
    val text: String?
)